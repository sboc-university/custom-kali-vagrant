# Vagrant Kali VM
![image](./build/ansible/files/wallpaper.png)
## Table Of Contents
- [Vagrant Kali VM](#vagrant-kali-vm)
	- [Table Of Contents](#table-of-contents)
	- [Description](#description)
	- [Requirements](#requirements)
	- [Install](#install)
		- [Nix](#nix)
		- [Winderps](#winderps)
	- [Configuration](#configuration)
	- [First Run](#first-run)
	- [Login Credentials](#login-credentials)
	- [Shutdown and restart the machine](#shutdown-and-restart-the-machine)
	- [Destroy Instance](#destroy-instance)
	- [Update](#update)
	- [Conclusion](#conclusion)

## Description
This is a pre-configured Kali VM. It is based on the official Kali Linux Rolling image that is released every quarter. 

## Requirements
- `virtualbox`
- `vagrant` (see install instructions below)
- A host machien that can handle a 2 vCPU and 4 GB machine
- 10 GB to download the base image 
- 40 GB for each instance of the box running
  - This is configurable either larger or smaller in either virtualbox/vmware or the vagrantfile
  - The base setup will expect at least 40 GB of disk space free.
## Install
### Nix
If your host machine is a linux machine install the vagrant package. [Web Directions](https://www.vagrantup.com/downloads)
```bash
curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt-get update && sudo apt-get install vagrant
```

### Winderps
If you are on a windows machine the following two commands must be run in an Administrative Powershell environment in order to ensure that you have the neccessary tooling to run the box on a windows environment. 

```powershell
Set-ExecutionPolicy Bypass -Scope Process -Force; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
cinst virtualbox vagrant
```
## Configuration
The box may be run with either Virtualbox or VMWare Workstation provider however you must pay for the Vagrant add-on in order to use the VMWare Provider.

The following commands should be run in the directory where you want your vagrant box to be downloaded and stored. These two commands will initiate the envrionment by downloading a Vagrantfile which can be used to define things like the amount of CPU and Memory to allocate to the box. The defaults should be fine for most situations (2vCPU/4GB). 
```powershell
vagrant init msmythe/dragons
vagrant up --provider virtualbox
```
## First Run
The first of run `vagrant up` can take anywhere between 15 min to 1 hr. The base image of the box is 10GB, but only needs to be downloaded once. After that subsequent runs simply clone the base box locally. In order to pull in updates to the box see the update section.
## Login Credentials
```
Username: vagrant
Password: vagrant
```

## Shutdown and restart the machine
In order to shutdown the machine you may run the following:
```powershell
vagrant down 
```
If you would like to bring the machine back up you may run the following:
```
vagrant up
```
You may of course use either virtualbox/vmwares gui to shutdown and restart the box as well. 
## Destroy Instance
In order to completely remove the instance of the machine you have been utilizing you may run the following command:
```powershell
vagrant destroy -f
``` 

Subsequent runs of the following two commands will check for updates to our customized image and boot a fresh copy of the image currently installed. If there has been an update it will signal that you can download the new image via another command (see below). 

```powershell
vagrant init -f msmythe/dragons
vagrant up --provider virtualbox
```
## Update
This will pull in any updates for the specified box in your Vagrantfile.
```powershell
vagrant box update
```
## Conclusion
If anyone would like to contribute to the project/notices anything that is missing/could be done better please let us know and we can integrate it and release it as an update to the box. For those that are interested in exactly how the pre-configured image is different from the base kalilinux/rolling box you can view the packer/ansible files located under the build directory in this repo.
