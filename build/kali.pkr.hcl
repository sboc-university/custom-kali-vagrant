variable "token" {
  type = string
}

variable "version" {
  type = string
}

source "vagrant" "virtualbox" {
  add_force     = true
  communicator  = "ssh"
  source_path   = "D:/virtualbox_kali_2021.3.0.box"
  provider      = "virtualbox"
}

source "vagrant" "vmware_desktop" {
  add_force     = true
  communicator  = "ssh"
  source_path   = "D:/vmware_kali_2021.3.0.box"
  provider      = "vmware_desktop"
}

build {
  sources = [
    "source.vagrant.virtualbox",
    "source.vagrant.vmware_desktop"
  ]

  provisioner "shell" {
    script = "./install_ansible.sh"
  }

  provisioner "ansible-local" {
    clean_staging_directory = true
    extra_arguments         = ["-vvv -e \"ansible_python_interpreter=/usr/bin/python3\""]
    galaxy_file             = "./ansible/galaxy-requirements.yml"
    playbook_dir            = "./ansible"
    playbook_file           = "./ansible/main.yml"
  }

  provisioner "shell" {
    script = "./integrate_into_ansible_later.sh"
  }

  provisioner "breakpoint" {
    disable       = false
    note          = "Clean up the desktop environment and run your manual checks before continuing."
  }

  post-processor "vagrant-cloud" {
    access_token  = var.token
    version       = var.version
    box_tag       = "msmythe/dragons"
    no_release    = true
  }
}
