#!/bin/bash
# Set the entire /opt directory to be owned by the vagrant user
sudo chown -R vagrant:vagrant /opt

# Download and install pycharm
wget -O /opt/pycharm.tar.gz https://download.jetbrains.com/python/pycharm-community-2021.2.2.tar.gz?_gl=1*1dek0rc*_ga*MTQzNzM5MjMzMS4xNjMxNjQ5NTYy*_ga_V0XZL7QHEB*MTYzMTgxMTU1MC42LjAuMTYzMTgxMTU1MC4w
mkdir /opt/pycharm
tar -xzf /opt/pycharm.tar.gz --strip-components=1 -C /opt/pycharm
rm /opt/pycharm.tar.gz

# Download and install ghidra & jdk11
## Install ghidra first
wget -O /opt/ghidra.zip https://github.com/NationalSecurityAgency/ghidra/releases/download/Ghidra_10.0.3_build/ghidra_10.0.3_PUBLIC_20210908.zip
mkdir /opt/ghidra
unzip /opt/ghidra.zip -d /opt/ghidra
mv /opt/ghidra/*/* /opt/ghidra
rm -rf /opt/ghidra/ghidra_*
ln -s /opt/ghidra/ghidraRun /opt/ghidra/ghidra
rm /opt/ghidra.zip

## Install jdk11
wget -O /opt/jdk11.tar.gz https://github.com/adoptium/temurin11-binaries/releases/download/jdk-11.0.12%2B7/OpenJDK11U-jdk_x64_linux_hotspot_11.0.12_7.tar.gz
mkdir /opt/jdk11
tar -xzf /opt/jdk11.tar.gz --strip-components=1 -C /opt/jdk11
rm /opt/jdk11.tar.gz

## Add cutter image for usage
mkdir /opt/cutter
wget -O /opt/cutter/cutter.svg https://raw.githubusercontent.com/rizinorg/cutter/14c57f5daacc45b940f20b0a8e6e95e24bc75da9/src/img/cutter.svg

# Dos2Unix all files that may be interacted with
dos2unix ~/.zshrc
dos2unix ~/.vimrc
dos2unix ~/.tmux.conf
dos2unix ~/htb-invite-code.py
sudo dos2unix /usr/bin/gdb-*
