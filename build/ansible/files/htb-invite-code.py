#!/usr/bin/env python3
import requests
import base64
import json

url = 'https://www.hackthebox.eu/api/invite/generate'
headers = {'Content-type': 'application/json; charset=utf-8', 'Accept': 'text/json', 'user-agent' : 'anything!'}
payload = {'key': 'value'}

r = requests.post(url, data=json.dumps(payload), headers=headers)
c = base64.b64decode(r.json()['data']['code'])
print("[+] Invite Code:", str(c.decode("utf-8")))
print("[*] Please navigate to https://www.hackthebox.eu/invite and input the above invite code!")