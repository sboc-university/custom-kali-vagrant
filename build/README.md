# Building Boxes
1. Bring up the Vagrant Environment

We should always use the global_id instead of source_path option because if the ansible provisioner fails during the build we would have to re-download the box each time as compared to simply destroying the box and then reloading the box from our local machine. This saves us a lot of development time and enables us to make neccessary changes should our scripts/provisioner setup fail in the future. We can of course ensure that our kalilinux/rolling is the latest version but it is unlikely that kali will be updating inbetween a single build timeframe. Long term we could build a contorler program in order to streamline this build process that much faster, but it is of low importance. 
  ```powershell
  # Download the base vagrantfile
  vagrant init -f kalilinux/rolling
  # Depending on the provider we are building for first run the following
  vagrant up --provider virtualbox
  vagrant up --provider vmware_desktop
  # Grab the gloabl_id varaible for the kali.pkr.hcl packer file
  vagrant global-status
  # Package the box first 
  vagrant package --vagrantfile .\Vagrantfile <GLOBAL_STATUS>
  # Move the box into the proper locations
  mv ./package.box virtualbox_kali_2021.3.0.box
  mv ./package.box vmware_kali_2021.3.0.box
  ```
2. Manually update the packages initially because it may drop to an interactive prompt which ansible will struggle with.
  ```bash
  # Open a prompt on the newly stood up box and run the following
  unset HISTFILE; export HISTSIZE=0; export HISTFILESIZE=0;
  sudo apt update && sudo apt upgrade -y
  ```
3. Update the global_id and run the Packer build
  ```powershell
  # Run the build
  packer build -force -on-error=ask -var token="TOKEN" -var version="2021.3.2" kali.pkr.hcl 
  ```
4. Ensure that the box looks and functions as intended. You will likely need to do the following things by hand after the update to ensure the box is fully ready to be pushed up.  
  ```bash
  # Preserve the history file as much as possible
  unset HISTFILE; export HISTSIZE=0; export HISTFILESIZE=0;
  # Ensure Firefox is working and is primed with two runs so that plugins are loaded
  firefox
  firefox
  # Ensure that cutter is accessible
  cutter
  # History file
  history
  # Ensure that there is only one burpsuite on the box

  # Ensure that Desktop Icons all work
  ## This is most eaily accomplished by hand

  # Ensure that the panel buttons are properly loaded
  ## This is most easily accomplished by hand
  ```